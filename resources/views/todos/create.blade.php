@extends('layouts.app')
@section('content')
<h1>Create a new Task</h1>
<form method = 'post' action = "{{action('TodoController@store')}}" >
{{csrf_field()}}

<div class = "form-group">
    <label for = "title"> What do you need to do?</label>
    <input type= "text" class = "form-control" name = "title">
</div>
<input type = "submit" class = "form-control" name = "submit" value = "Save">
</form>
@endsection