@extends('layouts.app')

@section('content')
<h1>below is your todo list</h1>
<table>
<tr>
    <th>Task</th>
    <th>Status</th>
</tr>
    @foreach($todos as $todo)
<tr>
    <td> <a href = "{{route('todos.edit',$todo->id)}}">  {{$todo->title}}</a></td>
      
       
      
        <td>@if ($todo->status)
           <input type = 'checkbox' id ="{{$todo->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$todo->id}}">
       @endif
        </td>
</tr>
    @endforeach

        <td>
        <a href = "{{route('todos.create')}}">Create a new todo</a>
        </td>
</tr>
</table>
@endsection